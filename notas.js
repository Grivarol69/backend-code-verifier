/**
 * Instalar typescript y sus tipos
 * ? npm install -D typescript @types/express @types/node
 * 
 * Para instalar TSC (TypeScript Compiler) y generar los archivos necesarios
 * ? npx tsc --init
 * 
 * Instalar npm Concurrently para ejecutar comandos concurrentemente en el package.json
 * ? npm i -D concurrently
 * 
 *  Crear los scripts necesarios para ejecutar distintas configuraciones
 * 
 * * "scripts": {
 * *   "build": "npx tsc",
 * *   "start": "node dist/index.js",
 * *   "dev": "concurrently \" concurrently tsc --watch\"  \"nodemon -q dist/index.js\" ",
 * *   "test": "echo \"Error: no test specified\" && exit 1"
 * * }
 * 
 * Instalar webpack y sus herramientas
 * ? npm i -D webpack webpack-cli webpack-node-externals webpack-shell-plugin
 *
 * Instalar Jest para hacer testing y Eslint para incorporar reglas de còdigo
 * ? npm i -D eslint jest ts-jest @types/jest supertest 
 * 
 * Para iniciar Eslint correr lo siguiente por consola
 * ? npx eslint --init
 * 
 * primera pregunta elegir
 * ? to check syntax, find problems and enforce code style
 * 
 * segunda pregunta elegir
 * ? Javascript Modules (import/export)
 * 
 * tercera pregunta elegir
 * ? none of these
 * 
 * cuarta pregunta
 * ? Does your project use Typescript: yes
 * 
 * quinta pregunta
 * ? Where does your code run: node
 * 
 * sexta pregunta elegir
 * ? Use a popular style guide
 * 
 * septima pregunta
 * ? Standard: https://github.com
 * 
 * format config file: Json
 * 
 * Elegir Yes cuando pregunte si instala las dependencias necesarias
 * 
 * Finalizada la instalaciòn y configuraciòn de Eslint continuar con Jest
 * ? npx jest --init
 * ?respuestas: yes - yes - node - yes - babel - yes
 * 
 * en el archivo jest.config.ts descomentar la siguiente linea
 * 
 * *coveragePathIgnorePatterns: [
 * * "/node_modules/"
 * *],
 * 
 * crear la carpeta __tests__ con el archivo example.specs.ts
 * dentro de ese archivo escribir el primer test
 * 
 ** describe('My first Test Suite', () => {
 **   it('My first Test Case', () => {
 **      expect(true).toBe(true);
 **    });
 ** });
 * Luego correrlo con
 * ? npm run test
 * 
 * Instalar 
 * ? npm i -D serve
 */