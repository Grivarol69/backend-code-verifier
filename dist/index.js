"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration .env file
dotenv_1.default.config();
// Create an Express App
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define first Route
app.get('/', (req, res) => {
    res.send('Welcome to the API Restful: NodeJs + Express + Jest + Swagger running succesfully!');
});
app.get('/hello', (req, res) => {
    res.send('Welcome to GET route: Hello World');
});
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map