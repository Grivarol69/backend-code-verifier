import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

// Configuration .env file
dotenv.config();

// Create an Express App
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define first Route
app.get('/', (req:Request, res:Response) => {
    res.send('Welcome to the API Restful: NodeJs + Express + Jest + Swagger running succesfully!');
});

app.get('/hello', (req: Request, res: Response) => {
    res.send('Welcome to GET route: Hello World');
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});

